package ru.nirinarkhova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.enumerated.Role;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-update-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "update a project by index.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER PROJECT INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Optional<Project> project = serviceLocator.getProjectService().findByIndex(userId, index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("[ENTER PROJECT NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER PROJECT DESCRIPTION:]");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Optional<Project> projectUpdate = serviceLocator.getProjectService().updateByIndex(userId, index, name, description);
        Optional.ofNullable(projectUpdate).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}

