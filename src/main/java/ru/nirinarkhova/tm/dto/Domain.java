package ru.nirinarkhova.tm.dto;

import lombok.Getter;
import lombok.Setter;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.model.User;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Domain implements Serializable {

    private List<Project> projects;

    private List<Task> tasks;

    private List<User> users;

}
