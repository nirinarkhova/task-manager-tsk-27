package ru.nirinarkhova.tm.exception.entity;

import ru.nirinarkhova.tm.exception.AbstractException;

public class ObjectNotFoundException extends AbstractException {

    public ObjectNotFoundException() {
        super("Error! Object not found!");
    }

}
