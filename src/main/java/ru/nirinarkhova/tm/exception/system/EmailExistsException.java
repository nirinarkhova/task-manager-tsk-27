package ru.nirinarkhova.tm.exception.system;

import ru.nirinarkhova.tm.exception.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }

}
